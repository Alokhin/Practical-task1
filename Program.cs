﻿using System;

namespace ConsoleCalculator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var calc = new Calculator();
            calc.Start();
        }
    }
}