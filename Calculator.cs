﻿using System;
using System.Collections.Generic;

namespace ConsoleCalculator
{
    public class Calculator
    {
        private Dictionary<string, Func<double, double, double>> operations;

        public Calculator()
        {
            operations = new Dictionary<string, Func<double, double, double>>()
            {
                {"+", (a, b) => a + b},
                {"-", (a, b) => a - b},
                {"*", (a, b) => a * b},
                {
                    "/", (a, b) =>
                    {
                        if (b == 0) throw new Exception("You can't divide by zero.");
                        return a / b;
                    }
                }
            };
        }
        
        public Calculator(Dictionary<string, Func<double, double, double>> operations)
        {
            this.operations = operations;
        }

        public void Start()
        {
            try
            {
                double x = EnterNumber("Enter x:");
                double y = EnterNumber("Enter y:");
                string op = EnterOperation();
                double res = Calculate(x, y, op);
                Console.WriteLine("{0} {1} {2} = {3}", x, op, y, res);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                Console.WriteLine(" Try it again:");
                Start();
            }
        }

        public double Calculate(double x, double y, string op)
        {
            return operations[op](x, y);
        }

        private double EnterNumber(string message)
        {
            double number;
            Console.WriteLine(message);
            string str = Console.ReadLine();
            while (!double.TryParse(str,out number))
            {
                Console.WriteLine("Bad number. Enter it again:");
                str = Console.ReadLine();
            }
            return number;
        }

        private string EnterOperation()
        {
            Console.WriteLine("Enter operation ({0})", string.Join(", ", operations.Keys));
            string operation = Console.ReadLine();
            while (!operations.ContainsKey(operation))
            {
                Console.WriteLine("Bad operation. Enter it again:");
                operation = Console.ReadLine();
            }
            return operation;
        }
    }
}